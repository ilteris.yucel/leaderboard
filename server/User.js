const { v4: uuidv4 } = require('uuid');
module.exports = class User {
  constructor(country, userName, rank, money, dailyDiff){
    this.userID = uuidv4();
    this.country = country;
    this.userName = userName;
    this.rank = rank;
    this.money = money;
    this.dailyDiff = dailyDiff;
  }
}

