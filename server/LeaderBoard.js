const Redis = require('ioredis');
const HOST = '127.0.0.1';
const REDIS_PORT = 6379;
const SET_KEY = 'leaderboard';

class LeaderBoard {
  constructor(host = HOST, port = REDIS_PORT, setKey = SET_KEY){
    this.setKey = setKey;
    this.client = new Redis({
      host: HOST,
      port: REDIS_PORT,
    });
    this.client.on('connect', () => {
      console.log(`Redis client connect on host: ${this.client.options.host} port: ${this.client.options.port}`);
    });
    this.client.on('error', (err) => {
      console.log(`${err} on redis client`);
    });
  };
  async addUser(score, obj){
    const status = await this.client.zadd(this.setKey, score, obj);
    return status;
  }
  async setUser(score, user){
    const status = await this.client.set(user, score);
    return status;
  }
  async getAllUser(){
    const result = await this.client.zrange(this.setKey, 0, -1);
    return result;
  }
}

module.exports.LeaderBoard = LeaderBoard;